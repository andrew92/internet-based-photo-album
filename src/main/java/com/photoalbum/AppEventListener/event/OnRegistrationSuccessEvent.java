package com.photoalbum.AppEventListener.event;

import com.photoalbum.dto.User;
import org.springframework.context.ApplicationEvent;

import java.util.Locale;

/**
 * Created by Andrzej Olkiewicz on 2015-11-11.
 */
public class OnRegistrationSuccessEvent extends ApplicationEvent {

    private String appUrl;
    private Locale locale;
    private User user;
    private boolean invoked = false;

    public OnRegistrationSuccessEvent(String appUrl, Locale locale, User user) {
        super(user);
        this.appUrl = appUrl;
        this.locale = locale;
        this.user = user;
    }

    // getters, setters
    public String getAppUrl() {
        return appUrl;
    }

    public void setAppUrl(String appUrl) {
        this.appUrl = appUrl;
    }

    public Locale getLocale() {
        return locale;
    }

    public void setLocale(Locale locale) {
        this.locale = locale;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public boolean isInvoked() {
        return invoked;
    }

    public void setInvoked(boolean invoked) {
        this.invoked = invoked;
    }
}
