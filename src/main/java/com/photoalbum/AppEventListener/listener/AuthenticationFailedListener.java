package com.photoalbum.AppEventListener.listener;

import com.photoalbum.service.LoginAttemptServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.security.authentication.event.AuthenticationFailureBadCredentialsEvent;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.stereotype.Component;

/**
 * Created by Andrzej Olkiewicz on 2015-11-13.
 */
@Component
public class AuthenticationFailedListener implements ApplicationListener<AuthenticationFailureBadCredentialsEvent>{

    @Autowired
    private LoginAttemptServiceImpl loginAttemptService;

    @Override
    public void onApplicationEvent(AuthenticationFailureBadCredentialsEvent event) {
        WebAuthenticationDetails authenticationDetails = (WebAuthenticationDetails) event.getAuthentication().getDetails();

        if(authenticationDetails != null) {
            loginAttemptService.loginFailed(authenticationDetails.getRemoteAddress());
        }
    }
}
