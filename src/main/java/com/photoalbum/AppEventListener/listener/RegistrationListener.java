package com.photoalbum.AppEventListener.listener;

import com.photoalbum.AppEventListener.event.OnRegistrationSuccessEvent;
import com.photoalbum.dto.User;
import com.photoalbum.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.MessageSource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

import java.util.UUID;

/**
 * Created by Andrzej Olkiewicz on 2015-11-11.
 */
@Component
public class RegistrationListener implements ApplicationListener<OnRegistrationSuccessEvent> {

    @Autowired
    UserService userService;

    @Autowired
    MessageSource messages;

    @Autowired
    JavaMailSender javaMailSender;

    @Override
    public void onApplicationEvent(OnRegistrationSuccessEvent event) {
        if(!event.isInvoked())
            this.confirmationEmail(event);

        event.setInvoked(true);
    }

    private void confirmationEmail(OnRegistrationSuccessEvent event){
        User user = event.getUser();
        String token = UUID.randomUUID().toString();
        userService.createUserVerificationToken(user, token);

        SimpleMailMessage email = buildEmailMessage(event, user, token);

        javaMailSender.send(email);
    }

    private SimpleMailMessage buildEmailMessage(OnRegistrationSuccessEvent event, User user, String token) {
        String sentToAddress = user.getEmail();
        String subject = messages.getMessage("RegisterConfirmEmail.Subject", null, event.getLocale());
        String confirmURL = event.getAppUrl() + "/user/confirmRegistration?token=" + token;
        String emailMessage = messages.getMessage("RegisterConfirmEmail.Message", new Object[]{confirmURL}, event.getLocale());


        SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
        simpleMailMessage.setTo(sentToAddress);
        simpleMailMessage.setSubject(subject);
        simpleMailMessage.setText(emailMessage);
        simpleMailMessage.setFrom("support@photoalbum.com");
        return simpleMailMessage;
    }
}
