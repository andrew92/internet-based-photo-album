package com.photoalbum.dao;

import com.photoalbum.dto.Gallery;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Andrzej Olkiewicz on 2015-11-19.
 */
public interface GalleryRepository extends JpaRepository<Gallery, Long>{
    Gallery findById(Long id);
}
