package com.photoalbum.dao;

import com.photoalbum.dto.PasswordResetToken;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Andrzej Olkiewicz on 2015-11-03.
 */
public interface PasswordResetTokenRepository extends JpaRepository<PasswordResetToken, Long> {
    PasswordResetToken findByToken(String token);
}
