package com.photoalbum.dao;

import com.photoalbum.dto.PhotoAlbumPage;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Andrzej Olkiewicz on 2015-12-21.
 */
public interface PhotoAlbumPageRepository extends JpaRepository<PhotoAlbumPage, Long>{
}
