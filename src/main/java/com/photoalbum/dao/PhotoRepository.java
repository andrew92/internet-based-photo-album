package com.photoalbum.dao;

import com.photoalbum.dto.Photo;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Andrzej Olkiewicz on 2015-11-19.
 */
public interface PhotoRepository extends JpaRepository<Photo, Long>{
    Photo findById(Long photoId);
}
