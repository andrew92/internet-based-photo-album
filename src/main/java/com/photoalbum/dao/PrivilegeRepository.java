package com.photoalbum.dao;

import com.photoalbum.dto.Privilege;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Andrzej Olkiewicz on 2015-11-03.
 */
public interface PrivilegeRepository extends JpaRepository<Privilege, Long> {
}
