package com.photoalbum.dao;

import com.photoalbum.dto.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Andrzej Olkiewicz on 2015-11-03.
 */
public interface UserRepository extends JpaRepository<User, Long> {
    User findByEmail(String email);
}
