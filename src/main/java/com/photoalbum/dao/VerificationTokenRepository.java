package com.photoalbum.dao;

import com.photoalbum.dto.VerificationToken;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Andrzej Olkiewicz on 2015-11-03.
 */
public interface VerificationTokenRepository extends JpaRepository<VerificationToken, Long> {
    VerificationToken findByToken(String token);
    VerificationToken findByUser_id(long user_id);
}
