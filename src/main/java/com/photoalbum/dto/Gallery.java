package com.photoalbum.dto;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import java.util.Collection;

/**
 * Created by Andrzej Olkiewicz on 2015-11-19.
 */
@Entity
@Table(name = "Gallery")
@AttributeOverride(name = "id", column = @Column(name = "GalleryID"))
/*@NamedQueries({
        @NamedQuery(
                name = "",
                query = ""
        )
}) */
public class Gallery extends BaseEntity{

    @Column(name = "GalleryName")
    @NotEmpty
    String GalleryName;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "galleries_photos", joinColumns = @JoinColumn(name = "gallery_id", referencedColumnName = "GalleryID"), inverseJoinColumns = @JoinColumn(name = "photo_id", referencedColumnName = "PhotoID"))
    Collection<Photo> images;

    // getters, setters

    public String getGalleryName() {
        return GalleryName;
    }

    public void setGalleryName(String galleryName) {
        GalleryName = galleryName;
    }

    public Collection<Photo> getImages() {
        return images;
    }

    public void setImages(Collection<Photo> images) {
        this.images = images;
    }

    public void addPhoto(Photo photo) {
        images.add(photo);
    }

    // hashcode, equals

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Gallery gallery = (Gallery) o;

        if (GalleryName != null ? !GalleryName.equals(gallery.GalleryName) : gallery.GalleryName != null) return false;
        return !(images != null ? !images.equals(gallery.images) : gallery.images != null);
    }

    @Override
    public int hashCode() {
        int result = GalleryName != null ? GalleryName.hashCode() : 0;
        result = 31 * result + (images != null ? images.hashCode() : 0);
        return result;
    }
}
