package com.photoalbum.dto;

import javax.persistence.*;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Andrzej Olkiewicz on 2015-11-03.
 */
@Entity
@Table(name = "PasswordResetToken")
@AttributeOverride(name = "id", column = @Column(name = "PasswordResetTokenID"))
/* @NamedQueries({
        @NamedQuery(
                name = "",
                query = ""
        )
}) */
public class PasswordResetToken extends BaseEntity {

    private final static int EXPIRATIONTIME = 60 * 24;

    @Column(name = "PasswordResetToken", unique = true)
    private String token;

    @Column(name = "ExpiryDate")
    private Date expiryDate;

    @OneToOne(targetEntity = User.class, fetch = FetchType.EAGER)
    @JoinColumn(nullable = false, name = "user_id")
    private User user;

    public PasswordResetToken() {
        super();
    }

    public PasswordResetToken(final String token) {
        super();
        this.token = token;
        this.expiryDate = expiryDateCalculator(EXPIRATIONTIME);
    }

    public PasswordResetToken(final String token, final User user) {
        super();
        this.token = token;
        this.expiryDate = expiryDateCalculator(EXPIRATIONTIME);
        this.user = user;
    }

    private Date expiryDateCalculator(final int expiryTimeInMinutes) {
        final Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(new Date().getTime());
        calendar.add(Calendar.MINUTE, expiryTimeInMinutes);
        return new Date(calendar.getTime().getTime());
    }

    public void updateToken(final String token)
    {
        this.token = token;
        this.expiryDate = expiryDateCalculator(EXPIRATIONTIME);
    }

    // getters, setters

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Date getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    // hashcode, equals method

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PasswordResetToken that = (PasswordResetToken) o;

        if (token != null ? !token.equals(that.token) : that.token != null) return false;
        if (expiryDate != null ? !expiryDate.equals(that.expiryDate) : that.expiryDate != null) return false;
        return !(user != null ? !user.equals(that.user) : that.user != null);

    }

    @Override
    public int hashCode() {
        int result = token != null ? token.hashCode() : 0;
        result = 31 * result + (expiryDate != null ? expiryDate.hashCode() : 0);
        result = 31 * result + (user != null ? user.hashCode() : 0);
        return result;
    }

    // toString method

    @Override
    public String toString() {
        return "PasswordResetToken{" +
                "token='" + token + '\'' +
                ", expiryDate=" + expiryDate +
                ", user=" + user +
                '}';
    }
}
