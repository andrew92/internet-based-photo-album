package com.photoalbum.dto;

import javax.persistence.*;
import java.sql.Blob;
import java.util.Arrays;

/**
 * Created by Andrzej Olkiewicz on 2015-11-19.
 */
@Entity
@Table(name = "Photo")
@AttributeOverride(name = "id", column = @Column(name = "PhotoID"))
/*@NamedQueries({
        @NamedQuery(
                name = "",
                query = ""
        )
}) */
public class Photo extends BaseEntity{

    @Lob
    private byte[] file;

    private String mimeType;

    private String fileName;

    public Photo() {

    }

    public Photo(String fileName, String mimeType, byte[] bytes) {
        this.fileName = fileName;
        this.mimeType = mimeType;
        this.file = bytes;
    }

    // getters, setters
    public byte[] getFile() {
        return file;
    }

    public void setFile(byte[] file) {
        this.file = file;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    // hashCode, equals
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Photo photo = (Photo) o;

        if (!Arrays.equals(file, photo.file)) return false;
        if (mimeType != null ? !mimeType.equals(photo.mimeType) : photo.mimeType != null) return false;
        return !(fileName != null ? !fileName.equals(photo.fileName) : photo.fileName != null);

    }

    @Override
    public int hashCode() {
        int result = file != null ? Arrays.hashCode(file) : 0;
        result = 31 * result + (mimeType != null ? mimeType.hashCode() : 0);
        result = 31 * result + (fileName != null ? fileName.hashCode() : 0);
        return result;
    }
}
