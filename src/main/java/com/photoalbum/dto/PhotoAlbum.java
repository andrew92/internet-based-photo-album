package com.photoalbum.dto;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import java.util.Collection;

/**
 * Created by Andrzej Olkiewicz on 2015-12-21.
 */
@Entity
@Table(name = "PhotoAlbum")
@AttributeOverride(name = "id", column = @Column(name = "PhotoAlbumID"))
/*@NamedQueries({
        @NamedQuery(
                name = "",
                query = ""
        )
}) */
public class PhotoAlbum extends BaseEntity{

    @Column(name = "PhotoAlbumName")
    @NotEmpty
    String PhotoAlbumName;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "photoalbums_photoalbumpages", joinColumns = @JoinColumn(name = "photoalbum_id", referencedColumnName = "PhotoAlbumID"), inverseJoinColumns = @JoinColumn(name = "photoalbumpage_id", referencedColumnName = "PhotoAlbumPageID"))
    Collection<PhotoAlbumPage> photoAlbumPages;

    public String getPhotoAlbumName() {
        return PhotoAlbumName;
    }

    public void setPhotoAlbumName(String photoAlbumName) {
        PhotoAlbumName = photoAlbumName;
    }

    public Collection<PhotoAlbumPage> getPhotoAlbumPages() {
        return photoAlbumPages;
    }

    public void setPhotoAlbumPages(Collection<PhotoAlbumPage> photoAlbumPages) {
        this.photoAlbumPages = photoAlbumPages;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PhotoAlbum that = (PhotoAlbum) o;

        if (PhotoAlbumName != null ? !PhotoAlbumName.equals(that.PhotoAlbumName) : that.PhotoAlbumName != null)
            return false;
        return !(photoAlbumPages != null ? !photoAlbumPages.equals(that.photoAlbumPages) : that.photoAlbumPages != null);

    }

    @Override
    public int hashCode() {
        int result = PhotoAlbumName != null ? PhotoAlbumName.hashCode() : 0;
        result = 31 * result + (photoAlbumPages != null ? photoAlbumPages.hashCode() : 0);
        return result;
    }
}
