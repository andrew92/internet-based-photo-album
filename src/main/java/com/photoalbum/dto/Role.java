package com.photoalbum.dto;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import java.util.Collection;

/**
 * Created by Andrzej Olkiewicz on 2015-10-25.
 */
@Entity
@Table(name = "Role")
@AttributeOverride(name = "id", column = @Column(name = "RoleID"))
/* @NamedQueries({
        @NamedQuery(
                name = "",
                query = ""
        )
}) */
public class Role extends BaseEntity {
    @Column(name = "RoleName", unique = true)
    @NotEmpty
    private String name;

    @Column(name = "Description")
    @NotEmpty
    private String description;

    @ManyToMany(mappedBy = "roles")
    private Collection<User> users;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "roles_privileges", joinColumns = @JoinColumn(name = "role_id", referencedColumnName = "RoleID"), inverseJoinColumns = @JoinColumn(name = "privilege_id", referencedColumnName = "PrivilegeID"))
    private Collection<Privilege> privileges;

    public Role() {
        super();
    }

    public Role(final String name, final String description) {
        super();
        this.name = name;
        this.description = description;
    }

    // getters, setters

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Collection<Privilege> getPrivileges() {
        return privileges;
    }

    public void setPrivileges(Collection<Privilege> privileges) {
        this.privileges = privileges;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Collection<User> getUsers() {
        return users;
    }

    public void setUsers(Collection<User> users) {
        this.users = users;
    }

    // hashcode, equals methods

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Role role = (Role) o;

        if (name != null ? !name.equals(role.name) : role.name != null) return false;
        if (description != null ? !description.equals(role.description) : role.description != null) return false;
        if (users != null ? !users.equals(role.users) : role.users != null) return false;
        return !(privileges != null ? !privileges.equals(role.privileges) : role.privileges != null);

    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (users != null ? users.hashCode() : 0);
        result = 31 * result + (privileges != null ? privileges.hashCode() : 0);
        return result;
    }

    // toString method

    /*
    @Override
    public String toString() {
        return "Role{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", users=" + users +
                ", privileges=" + privileges +
                '}';
    } */
}
