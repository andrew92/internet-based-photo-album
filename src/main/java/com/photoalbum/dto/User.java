package com.photoalbum.dto;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import java.util.Collection;

/**
 * Created by Andrzej Olkiewicz on 2015-10-25.
 */
@Entity
@Table(name = "User")
@AttributeOverride(name = "id", column = @Column(name = "UserID"))
/*@NamedQueries({
        @NamedQuery(
                name = "",
                query = ""
        )
}) */
public class User extends BaseEntity {

    @Column(name = "FirstName")
    @NotEmpty
    private String firstName;

    @Column(name = "LastName")
    @NotEmpty
    private String lastName;

    @Column(name = "Email")
    @NotEmpty
    private String email;

    @Column(name = "Password", length = 60)
    private String password;

    private boolean enabled;

    private boolean tokenExpired;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "users_roles", joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "UserID"), inverseJoinColumns = @JoinColumn(name = "role_id", referencedColumnName = "RoleID"))
    private Collection<Role> roles;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "users_galleries", joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "UserID"), inverseJoinColumns = @JoinColumn(name = "gallery_id", referencedColumnName = "GalleryID"))
    private Collection<Gallery> galleries;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "users_photoAlbums", joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "UserID"), inverseJoinColumns = @JoinColumn(name = "photoAlbum_id", referencedColumnName = "PhotoAlbumID"))
    private Collection<PhotoAlbum> photoAlbums;

    public User() {
        super();
        this.enabled = false;
        this.tokenExpired = false;
    }


    public void addGallery(Gallery gallery){
        galleries.add(gallery);
    }

    public void addPhotoAlbum(PhotoAlbum photoAlbum) {
        photoAlbums.add(photoAlbum);
    }

    //getter, setter

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public Collection<Role> getRoles() {
        return roles;
    }

    public void setRoles(Collection<Role> roles) {
        this.roles = roles;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public boolean isTokenExpired() {
        return tokenExpired;
    }

    public void setTokenExpired(boolean tokenExpired) {
        this.tokenExpired = tokenExpired;
    }

    public Collection<Gallery> getGalleries() {
        return galleries;
    }

    public void setGalleries(Collection<Gallery> galleries) {
        this.galleries = galleries;
    }

    public Collection<PhotoAlbum> getPhotoAlbums() {
        return photoAlbums;
    }

    public void setPhotoAlbums(Collection<PhotoAlbum> photoAlbums) {
        this.photoAlbums = photoAlbums;
    }


    // hashcode, equals methods

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        if (enabled != user.enabled) return false;
        if (tokenExpired != user.tokenExpired) return false;
        if (firstName != null ? !firstName.equals(user.firstName) : user.firstName != null) return false;
        if (lastName != null ? !lastName.equals(user.lastName) : user.lastName != null) return false;
        if (email != null ? !email.equals(user.email) : user.email != null) return false;
        if (password != null ? !password.equals(user.password) : user.password != null) return false;
        if (roles != null ? !roles.equals(user.roles) : user.roles != null) return false;
        if (galleries != null ? !galleries.equals(user.galleries) : user.galleries != null) return false;
        return !(photoAlbums != null ? !photoAlbums.equals(user.photoAlbums) : user.photoAlbums != null);

    }

    @Override
    public int hashCode() {
        int result = firstName != null ? firstName.hashCode() : 0;
        result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        result = 31 * result + (enabled ? 1 : 0);
        result = 31 * result + (tokenExpired ? 1 : 0);
        result = 31 * result + (roles != null ? roles.hashCode() : 0);
        result = 31 * result + (galleries != null ? galleries.hashCode() : 0);
        result = 31 * result + (photoAlbums != null ? photoAlbums.hashCode() : 0);
        return result;
    }


    // toString method
    /*
    @Override
    public String toString() {
        return "User{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", enabled=" + enabled +
                ", tokenExpired=" + tokenExpired +
                ", roles=" + roles +
                '}';
    } */
}
