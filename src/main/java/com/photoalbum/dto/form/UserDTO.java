package com.photoalbum.dto.form;

import com.photoalbum.validation.PasswordMatches;
import com.photoalbum.validation.ValidEmail;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by Andrzej Olkiewicz on 2015-11-03.
 */
@PasswordMatches
public class UserDTO {

    @NotNull
    @NotEmpty
    @Size(min = 1)
    private String firstName;

    @NotNull
    @NotEmpty
    @Size(min = 1)
    private String lastName;

    @ValidEmail
    @NotNull
    @NotEmpty
    @Size(min = 1)
    private String email;

    @NotNull
    @NotEmpty
    @Size(min = 1)
    private String password;

    @NotNull
    @NotEmpty
    @Size(min = 1)
    private String matchingPassword;

    // getters, setters

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMatchingPassword() {
        return matchingPassword;
    }

    public void setMatchingPassword(String matchingPassword) {
        this.matchingPassword = matchingPassword;
    }

    // hashcode, equals methods

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserDTO userDTO = (UserDTO) o;

        if (firstName != null ? !firstName.equals(userDTO.firstName) : userDTO.firstName != null) return false;
        if (lastName != null ? !lastName.equals(userDTO.lastName) : userDTO.lastName != null) return false;
        if (email != null ? !email.equals(userDTO.email) : userDTO.email != null) return false;
        if (password != null ? !password.equals(userDTO.password) : userDTO.password != null) return false;
        return !(matchingPassword != null ? !matchingPassword.equals(userDTO.matchingPassword) : userDTO.matchingPassword != null);

    }

    @Override
    public int hashCode() {
        int result = firstName != null ? firstName.hashCode() : 0;
        result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        result = 31 * result + (matchingPassword != null ? matchingPassword.hashCode() : 0);
        return result;
    }

    // toString method

    @Override
    public String toString() {
        return "UserDTO{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", matchingPassword='" + matchingPassword + '\'' +
                '}';
    }
}
