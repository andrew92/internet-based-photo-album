package com.photoalbum.service;

import com.photoalbum.dto.*;
import com.photoalbum.dto.form.UserDTO;

import java.sql.Blob;
import java.util.Collection;
import java.util.List;

/**
 * Created by Andrzej Olkiewicz on 2015-11-04.
 */
public interface UserService {

    //
    User createUser(UserDTO userDTO);
    void saveUser(User user);
    User getUser(String email);
    boolean isUserExists(String email);
    void createUserVerificationToken(User user, String token);
    VerificationToken getVerificationToken(String token);
    void createPasswordResetToken(User user, String token);
    PasswordResetToken getPasswordResetToken(String token);
    void updateUserPassword(User user, String password);
    void updateUserVerificationToken(User user, String token);
    Collection<Gallery> getUserGalleries(String email);
    Gallery setNewUserGallery(String email, String galleryName);
    Collection<Photo> getGalleryPhotos(Long GalleryId);
    void addNewPhoto(Long GalleryId, Photo photo);
    void deleteGallery(String userName, Long id);
    void deletePhoto(String userName, Long galleryId, Long id);
    Collection<PhotoAlbum> getUserPhotoAlbums(String email);
    PhotoAlbum setNewUserPhotoAlbum(String email, String photoAlbumName);
    PhotoAlbum getPhotoAlbum(Long id);
    void SaveAlbumPhotoPage(Long albumID, int photoPageID, String JSONPage, String CanvasToJPEG);
    void deletePhotoAlbum(String userName, Long id);
    List<PhotoAlbumPage> photoAlbumPages(Long id);
    void deletePhotoAlbumPage(Long albumID, Long id);
}
