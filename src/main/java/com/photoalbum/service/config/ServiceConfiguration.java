package com.photoalbum.service.config;

import com.photoalbum.service.LoginAttemptServiceImpl;
import com.photoalbum.service.UserDetailsServiceImpl;
import com.photoalbum.service.UserService;
import com.photoalbum.service.UserServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.Properties;

/**
 * Created by Admin on 2015-10-05.
 */
@Configuration
public class ServiceConfiguration {

    // @Bean klas service
    @Bean
    public UserService userService() {
        return new UserServiceImpl();
    }

    @Bean
    public UserDetailsService UserDetailsServiceImpl(){
        return new UserDetailsServiceImpl();
    }

    @Bean
    public LoginAttemptServiceImpl loginAttemptService(){
        return new LoginAttemptServiceImpl();
    }

    @Bean
    public JavaMailSenderImpl mailSender() {
        JavaMailSenderImpl javaMailSender = new JavaMailSenderImpl();

        javaMailSender.setUsername("PhotoAlbumPK@gmail.com");
        javaMailSender.setPassword("PhotoAlbum1234");
        javaMailSender.setProtocol("smtp");
        javaMailSender.setHost("smtp.gmail.com");
        javaMailSender.setPort(587);


        Properties props = new Properties();
        props.setProperty("mail.debug", "true");
        //props.setProperty("mail.user", "PhotoAlbumPK@gmail.com");
        //props.setProperty("mail.password", "PhotoAlbum1234");
        //props.setProperty("mail.transport.protocol", "smtp");
        //props.setProperty("mail.smtp.host", "smtp.gmail.com");
        props.setProperty("mail.smtp.auth", "true");
        //props.setProperty("mail.smtp.port", "587");
        props.setProperty("mail.smtp.starttls.enable", "true");
        props.setProperty("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");

        javaMailSender.setJavaMailProperties(props);

        return javaMailSender;
    }
}

