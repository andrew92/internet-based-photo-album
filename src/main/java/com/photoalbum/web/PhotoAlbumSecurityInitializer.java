package com.photoalbum.web;

import org.springframework.core.annotation.Order;
import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 * Created by Admin on 2015-10-04.
 */
@Order(2)
public class PhotoAlbumSecurityInitializer extends AbstractSecurityWebApplicationInitializer {
}
