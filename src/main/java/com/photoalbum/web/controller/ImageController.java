package com.photoalbum.web.controller;

import com.fasterxml.jackson.databind.deser.Deserializers;
import com.photoalbum.dao.PhotoAlbumPageRepository;
import com.photoalbum.dao.PhotoRepository;
import com.photoalbum.dto.Photo;
import com.photoalbum.dto.PhotoAlbumPage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Base64;

/**
 * Created by Andrzej Olkiewicz on 2015-11-25.
 */
@Controller
@RequestMapping(value = "/showImage")
public class ImageController {

    @Autowired
    PhotoRepository photoRepository;

    @Autowired
    PhotoAlbumPageRepository photoAlbumPageRepository;

    @RequestMapping(method = RequestMethod.GET)
    public void showImage(@RequestParam("id") Long photoId, HttpServletResponse response, HttpServletRequest request)
            throws ServletException, IOException {
        Photo photo = photoRepository.findById(photoId);
        response.setContentType("image/jpeg, image/jpg, image/png, image/gif");
        response.getOutputStream().write(photo.getFile());

        response.getOutputStream().close();
    }

    @RequestMapping(value = "/photoalbumpage", method = RequestMethod.GET)
    public void showPhotoAlbumPage(@RequestParam("id") Long photoId, HttpServletResponse response, HttpServletRequest request)
            throws ServletException, IOException {
        PhotoAlbumPage photoAlbumPage = photoAlbumPageRepository.findOne(photoId);
        String imageString = new String(photoAlbumPage.getFile());

        String imageDataBytes = imageString.substring(imageString.indexOf(",") + 1);
        byte[] img = Base64.getDecoder().decode(imageDataBytes.getBytes());

        response.setContentType("image/jpeg");
        response.getOutputStream().write(img);
        response.getOutputStream().flush();
        response.getOutputStream().close();
    }
}