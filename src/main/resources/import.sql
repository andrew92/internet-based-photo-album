/* ACCOUNT ROLES */
INSERT INTO `photoalbumdb`.`role` (`Description`, `RoleName`) VALUES ('User Account', 'ROLE_USER');
INSERT INTO `photoalbumdb`.`role` (`Description`, `RoleName`) VALUES ('Admin Account', 'ROLE_ADMIN');

/* ACCOUNT PRIVILEGES */
INSERT INTO `photoalbumdb`.`privilege` (`name`) VALUES ('USER_PRIVILEGE');
INSERT INTO `photoalbumdb`.`privilege` (`name`) VALUES ('ADMIN_PRIVILEGE');

/* DEFAULT ROLE-PRIVILEGES */
INSERT INTO `photoalbumdb`.`roles_privileges` (`role_id`, `privilege_id`) VALUES ('1', '1');
INSERT INTO `photoalbumdb`.`roles_privileges` (`role_id`, `privilege_id`) VALUES ('2', '2');