<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="jstl" %>

<%@ include file="libraries.jspf" %>

<!-- Additional values -->
<spring:message code="GalleryPage.SiteTitle" var="pageTitle"/>

<!DOCTYPE html>
<html>
<head>

  <link href="${SemanticCSS}" rel="stylesheet" />
  <link href="${themeCSS}" rel="stylesheet" />
    <link href="${DropZoneCSS}" rel="stylesheet" />
    <link href="${FancyBoxCSS}" rel="stylesheet" />

  <script src="${jQuery}"></script>
  <script src="${jQueryUI}"></script>
  <script src="${jQuerySerialize}"></script>
  <script src="${i18props}"></script>

    <script src="${DropZone}"></script>
    <script src="${FancyBoxJS}"></script>

  <script src="${SemanticJS}"></script>
  <script src="${themeJS}"></script>

  <title>${pageTitle}</title>

  <script>
    $(document).ready(function() {
        $(".fancybox").fancybox({
            type: "image",
            openEffect	: 'none',
            closeEffect	: 'none'
        });

        // "dropzoneForm" is the camel-case version of the form id "dropzone-form"
        Dropzone.options.dropzoneForm = {

            url : $(location).attr('href'), // not required if the <form> element has action attribute
            autoProcessQueue : false,
            uploadMultiple : true,
            maxFilesize : 256, // MB
            parallelUploads : 100,
            maxFiles : 100,
            addRemoveLinks : true,
            previewsContainer : ".dropzone-previews",

            // The setting up of the dropzone
            init : function() {

                var myDropzone = this;

                // first set autoProcessQueue = false
                $('#upload-button').on("click", function(e) {
                    e.preventDefault();
                    myDropzone.processQueue();

                });

                this.on("complete", function(file) {
                    location.reload();
                });

                // customizing the default progress bar
                this.on("uploadprogress", function(file, progress) {

                    progress = parseFloat(progress).toFixed(0);

                    var progressBar = file.previewElement.getElementsByClassName("dz-upload")[0];
                    progressBar.innerHTML = progress + "%";
                });
            }
        };

        $('#followMenu').hide();

        // fix menu when passed
        $('#MainHeader')
                .visibility({
                    once: false,
                    onBottomPassed: function () {
                        $('#followMenu').transition('fade in');
                    },
                    onBottomPassedReverse: function () {
                        $('#followMenu').transition('fade out');
                    }
                })
    });
  </script>
  <style>
    .ui.grid {
      margin-top: 20px;
      margin-bottom: 20px;
    }
    .floated.button {
      margin-right: 20px;
    }
    #wrapper {
        min-height: 100%;
    }
  </style>
</head>

<body>

<div id="wrapper">

<!-- Import Header-->
<%@ include file="header.jspf" %>


<div class="ui grid">
  <div class="four wide column">
    <div class="ui vertical fluid tabular menu">
      <a href="<c:url value="/user/controlPanel/"/>" class="item active">
        <spring:message code="UserControlPanelPage.TabGallery"/>
      </a>
      <a href="<c:url value="/user/controlPanel/PhotoAlbum/"/>" class="item">
        <spring:message code="UserControlPanelPage.TabPhotoAlbum"/>
      </a>
    </div>
  </div>
  <div class="twelve wide stretched column">
    <div class="ui clearing segment">
      <spring:message code="UserControlPanelPage.GalleryMessage"/>
    </div>

    <div class="ui three column grid" id="galleryGrid">

      <jstl:forEach items="${photos}" var="photo">
        <div class="column">
          <div class="ui image segment">
              <div class="image">
                  <a class="fancybox" rel="gallery1" href="<c:url value="/showImage?id=${photo.getId()}"/>" title="${photo.getFileName()}">
                      <img src="<c:url value="/showImage?id=${photo.getId()}"/>" alt="${photo.getFileName()}" />
                  </a>
              </div>
          </div>
            <a href="<c:url value="deletePhoto/${photo.getId()}"/>">
                <button class="negative ui button">
                    <spring:message code="Common.Delete"/>
                </button>
            </a>
        </div>
      </jstl:forEach>

    </div>

      <!--
      <div class="ui clearing segment" id="DropZone">
          <spring:message code="GalleryPage.DropZone"/>
      </div> -->

      <div class="ui stacked segment">
          <div class="ui compact message">
              <p><spring:message code="GalleryPage.DropZoneLabel"/></p>
          </div>

          <div class="panel-body">

              <div>

                  <form id="dropzone-form" class="dropzone"
                        enctype="multipart/form-data">

                      <div class="dz-default dz-message file-dropzone text-center well col-sm-12">
                          <spring:message code="GalleryPage.DropZone"/>
                      </div>

                      <!-- this is were the previews should be shown. -->
                      <div class="dropzone-previews"></div>
                  </form>

                  <hr>

                  <button id="upload-button" class="positive ui button">
                      <spring:message code="GalleryPage.UploadBtn"/>
                  </button>

              </div>

          </div>
      </div>
  </div>
</div>
</div>
<!-- Import Footer-->
<%@ include file="footer.jspf" %>

</body>
</html>
