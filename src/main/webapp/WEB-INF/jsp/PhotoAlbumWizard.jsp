<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="jstl" %>

<%@ include file="libraries.jspf" %>

<!-- Additional values -->
<spring:message code="UserControlPanelPage.SiteTitle" var="pageTitle"/>
<spring:url value="/resources/assets/noImage.png" var="noImage" />

<!DOCTYPE html>
<html>
<head>

  <link href="${SemanticCSS}" rel="stylesheet" />
  <link href="${themeCSS}" rel="stylesheet" />
  <link href="${SpectrumCSS}" rel="stylesheet" />
  <link href="${jQueryUICSS}" rel="stylesheet" />

  <script src="${jQuery}"></script>
  <script src="${jQueryUI}"></script>
  <script src="${jQuerySerialize}"></script>
  <script src="${i18props}"></script>
  <script src="${FabricJS}"></script>
  <script src="${SpectrumJS}"></script>

  <script src="${SemanticJS}"></script>
  <script src="${themeJS}"></script>

  <title>${pageTitle}</title>

  <script>
    $(document).ready(function() {

      $('#followMenu').hide();
      $('#SimpleShapesOptions').hide();
      $('#TextOptions').hide();
      $('#DrawingModeOptions').hide();

      $('.draggable').draggable({
          revert : function(event, ui) {
              // on older version of jQuery use "draggable"
              // $(this).data("draggable")
              // on 2.x versions of jQuery use "ui-draggable"
              // $(this).data("ui-draggable")
              $(this).data("ui-draggable").originalPosition = {
                  top : 0,
                  left : 0
              };


              // return boolean
              //return !event;
              return true;
              // that evaluate like this:
              // return event !== false ? false : true;
          }
      });

      // fix menu when passed
      $('#MainHeader')
              .visibility({
                once: false,
                onBottomPassed: function() {
                  $('#followMenu').transition('fade in');
                },
                onBottomPassedReverse: function() {
                  $('#followMenu').transition('fade out');
                }
              })
      ;

        // CANVAS
        resizeCanvas();

        var canvas = new fabric.Canvas('canvas');
        canvas.on('before:selection:cleared', function() {
            selectedClearedObjectEvent();
        });

        canvas.on('object:selected', selectedDrawingObject);

        // test
        var albumID = '${albumID}';
        var photoPage = '${photoPage}';
        var photoPageJSON = '${photoPageJSON}';

        console.log(albumID);
        console.log(photoPage);
        console.log(photoPageJSON);


        //canvas.loadFromJSON(photoPageJSON);
        //canvas.renderAll();

        canvas.loadFromJSON(photoPageJSON,canvas.renderAll.bind(canvas));

        if(photoPage.valueOf() <= 1)
        {
            $('#previousBtn').hide();
        }

        var photoPageID = parseInt(photoPage);

        var prevPage = parseInt(photoPage);
        prevPage--;
        var prevURL = "/PhotoAlbum/user/controlPanel/PhotoAlbum/" + albumID + "/" + prevPage + "/";

        var nextPage = parseInt(photoPage);
        nextPage++;
        var nextURL = "/PhotoAlbum/user/controlPanel/PhotoAlbum/" + albumID + "/" + nextPage + "/";

        $('#previousBtn').on('click', function(){
            var data = JSON.stringify(canvas);
            var canvasToJPEG = canvas.toDataURL({
                format: 'jpeg'
            });

            console.log(canvasToJPEG);

            var jsonData = JSON.stringify({
                "canvas": data,
                "albumID": albumID,
                "photoAlbumPageID": photoPageID,
                "canvasToJPEG": canvasToJPEG
            });
            console.log(jsonData);

            $.ajax({
                url: '<c:url value="/user/controlPanel/savePhotoPage"/>',
                type: "post",
                data: jsonData,
                dataType : "json",
                contentType: 'application/json; charset=utf-8',
                async: false,
                success : function(data){
                    console.log(data);
                },
                error : function(xhr, status){
                    console.log(status);
                }
            });

            $(location).attr('href', prevURL);
        });

        $('#nextBtn').on('click', function(){
            var data = JSON.stringify(canvas);
            var canvasToJPEG = canvas.toDataURL({
                format: 'jpeg'
            });

            console.log(canvasToJPEG);

            var jsonData = JSON.stringify({
                "canvas": data,
                "albumID": albumID,
                "photoAlbumPageID": photoPageID,
                "canvasToJPEG": canvasToJPEG
            });
            console.log(jsonData);

            $.ajax({
                url: '<c:url value="/user/controlPanel/savePhotoPage"/>',
                type: "post",
                data: jsonData,
                dataType : "json",
                contentType: 'application/json; charset=utf-8',
                async: false,
                success : function(data){
                    console.log(data);
                },
                error : function(xhr, status){
                    console.log(status);
                }
            });

            $(location).attr('href', nextURL);
        });
        //test

        $('#canvas').droppable({
            accept: '.draggable',
            drop: function( event, ui ) {

                //var imgElement = $("<img>").attr("alt", ui.draggable.attr('alt')).html();

                var imgElement = document.getElementById(ui.draggable.attr("id"));
                console.log(imgElement);
                var imgInstance = new fabric.Image(imgElement, {
                    left: 100,
                    top: 100,
                    angle: 30,
                    z:100
                });
                canvas.add(imgInstance);
                canvas.renderAll();

                console.log(ui.draggable.attr('alt'));
                console.log('dropped into canvas');
            }
        });

        // MENU TAB
        $('.menu .item')
                .tab()
        ;

        // SLIDER
        $("#slider").slider({
            animate: "slow",
            min: 0,
            max: 100,
            value: 100,
            orientation: "horizontal",
            slide: function( event, ui ) {
                var val = $('#slider').slider("option", "value");
                canvas.getActiveObject().setOpacity((val / 100));
                canvas.renderAll();
            }
        });

        $('#StrokeWidth').slider({
            animate: "slow",
            min: 0,
            max: 30,
            value: 20,
            orientation: "horizontal",
            slide: function( event, ui ) {
                var val = $('#StrokeWidth').slider("option", "value");
                canvas.getActiveObject().setStroke(val);
                canvas.renderAll();
            }
        });

        $('#FontSize').slider({
            animate: "slow",
            min: 0,
            max: 72,
            value: 16,
            orientation: "horizontal",
            slide: function( event, ui ) {
                var val = $('#FontSize').slider("option", "value");
                canvas.getActiveObject().setFontSize(val);
                canvas.renderAll();
            }
        });

        $('#LineHeight').slider({
            animate: "slow",
            min: 0,
            max: 5,
            value: 2,
            orientation: "horizontal",
            slide: function( event, ui ) {
                var val = $('#LineHeight').slider("option", "value");
                canvas.getActiveObject().setLineHeight(val);
                canvas.renderAll();
            }
        });

        $('#TextOpacity').slider({
            animate: "slow",
            min: 0,
            max: 100,
            value: 100,
            orientation: "horizontal",
            slide: function( event, ui ) {
                var val = $('#TextOpacity').slider("option", "value");
                console.log("Text Opacity" + val);
                canvas.getActiveObject().setOpacity((val / 100));
                canvas.renderAll();
            }
        });

        $('#LineWidth').slider({
            animate: "slow",
            min: 0,
            max: 100,
            value: 100,
            orientation: "horizontal",
            slide: function( event, ui ) {
                canvas.freeDrawingBrush.width = $('#LineWidth').slider("option", "value");
            }
        });

        $('#ShadowWidth').slider({
            animate: "slow",
            min: 0,
            max: 100,
            value: 100,
            orientation: "horizontal",
            slide: function( event, ui ) {
                canvas.freeDrawingBrush.shadowBlur = $('#ShadowWidth').slider("option", "value");
                canvas.freeDrawingBrush.shadowColor = 'rgb(0,0,0)';
            }
        });

        // DROPDOWN
        $('#FontFamilyDropdown').dropdown({
            onChange: function(value, text, $selectedItem) {
                canvas.getActiveObject().setFontFamily(value);
                canvas.renderAll();
                return;
            }
        });

        $('#TextAlignDropdown').dropdown({
            onChange: function(value, text, $selectedItem) {
                canvas.getActiveObject().setTextAlign(value);
                canvas.renderAll();
                return;
            }
        });

        var vLinePatternBrush = new fabric.PatternBrush(canvas);
        vLinePatternBrush.getPatternSrc = function() {

            var patternCanvas = fabric.document.createElement('canvas');
            patternCanvas.width = patternCanvas.height = 10;
            var ctx = patternCanvas.getContext('2d');

            ctx.strokeStyle = this.color;
            ctx.lineWidth = 5;
            ctx.beginPath();
            ctx.moveTo(0, 5);
            ctx.lineTo(10, 5);
            ctx.closePath();
            ctx.stroke();

            return patternCanvas;
        };

        var hLinePatternBrush = new fabric.PatternBrush(canvas);
        hLinePatternBrush.getPatternSrc = function() {

            var patternCanvas = fabric.document.createElement('canvas');
            patternCanvas.width = patternCanvas.height = 10;
            var ctx = patternCanvas.getContext('2d');

            ctx.strokeStyle = this.color;
            ctx.lineWidth = 5;
            ctx.beginPath();
            ctx.moveTo(5, 0);
            ctx.lineTo(5, 10);
            ctx.closePath();
            ctx.stroke();

            return patternCanvas;
        };

        var squarePatternBrush = new fabric.PatternBrush(canvas);
        squarePatternBrush.getPatternSrc = function() {

            var squareWidth = 10, squareDistance = 2;

            var patternCanvas = fabric.document.createElement('canvas');
            patternCanvas.width = patternCanvas.height = squareWidth + squareDistance;
            var ctx = patternCanvas.getContext('2d');

            ctx.fillStyle = this.color;
            ctx.fillRect(0, 0, squareWidth, squareWidth);

            return patternCanvas;
        };

        var diamondPatternBrush = new fabric.PatternBrush(canvas);
        diamondPatternBrush.getPatternSrc = function() {

            var squareWidth = 10, squareDistance = 5;
            var patternCanvas = fabric.document.createElement('canvas');
            var rect = new fabric.Rect({
                width: squareWidth,
                height: squareWidth,
                angle: 45,
                fill: this.color
            });

            var canvasWidth = rect.getBoundingRectWidth();

            patternCanvas.width = patternCanvas.height = canvasWidth + squareDistance;
            rect.set({ left: canvasWidth / 2, top: canvasWidth / 2 });

            var ctx = patternCanvas.getContext('2d');
            rect.render(ctx);

            return patternCanvas;
        };

        $('#DrawingEqDropdown').dropdown({
            onChange: function(value, text, $selectedItem) {
                if (value === 'hline') {
                    canvas.freeDrawingBrush = vLinePatternBrush;
                }
                else if (value === 'vline') {
                    canvas.freeDrawingBrush = hLinePatternBrush;
                }
                else if (value === 'square') {
                    canvas.freeDrawingBrush = squarePatternBrush;
                }
                else if (value === 'diamond') {
                    canvas.freeDrawingBrush = diamondPatternBrush;
                }
                else if (value === 'texture') {
                    canvas.freeDrawingBrush = texturePatternBrush;
                }
            }
        });

        // CHECKBOXES
        $('#TextBold').checkbox({
            onChecked: function() {
                canvas.getActiveObject().setFontWeight('bold');
                canvas.renderAll();
                return;
            },
            onUnchecked: function() {
                canvas.getActiveObject().setFontWeight('normal');
                canvas.renderAll();
                return;
            }
        });

        $('#TextItalic').checkbox({
            onChecked: function() {
                canvas.getActiveObject().setFontStyle('italic');
                canvas.renderAll();
                return;
            },
            onUnchecked: function() {
                canvas.getActiveObject().setFontStyle('normal');
                canvas.renderAll();
                return;
            }
        });

        $('#TextUnderline').checkbox({
            onChecked: function() {
                canvas.getActiveObject().setTextDecoration('underline');
                canvas.renderAll();
                return;
            },
            onUnchecked: function() {
                canvas.getActiveObject().setTextDecoration('');
                canvas.renderAll();
                return;
            }
        });

        $('#TextLineThrough').checkbox({
            onChecked: function() {
                canvas.getActiveObject().setTextDecoration('line-through');
                canvas.renderAll();
                return;
            },
            onUnchecked: function() {
                canvas.getActiveObject().setTextDecoration('');
                canvas.renderAll();
                return;
            }
        });

        $('#TextOverLine').checkbox({
            onChecked: function() {
                canvas.getActiveObject().setTextDecoration('overline');
                canvas.renderAll();
                return;
            },
            onUnchecked: function() {
                canvas.getActiveObject().setTextDecoration('');
                canvas.renderAll();
                return;
            }
        });

        // Delete selected Btn
        $('#deleteSelectedBtn').on('click', function(){
            console.log("Usuwam kuzwa!");
            canvas.getActiveObject().remove();
        });

        // Clear canvas
        $('#clearCanvasBtn').on('click', function(){
            selectedClearedObjectEvent();
            canvas.clear();
        });

        // Drawing Mode
        $('#drawingModeBtn').on('click', function(){
            canvas.isDrawingMode = !canvas.isDrawingMode;

            if(canvas.isDrawingMode == true){
                $('#DrawingModeOptions').show();
            } else{
                $('#DrawingModeOptions').hide();
            }
        });

        // Simple shapes
        $('#rectangleBtn').on('click', function(){
            var rect = new fabric.Rect({
                left: 100,
                top: 100,
                fill: 'red',
                width: 20,
                height: 20
            });

            rect.on('selected', selectedObjectEvent);
            canvas.add(rect);
        });

        $('#circleBtn').on('click', function(){
            var circle = new fabric.Circle({
                radius: 20, fill: 'green', left: 100, top: 100
            });

            circle.on('selected', selectedObjectEvent);
            canvas.add(circle);
        });

        $('#triangleBtn').on('click', function(){
            var triangle = new fabric.Triangle({
                width: 20, height: 30, fill: 'blue', left: 50, top: 50
            });

            triangle.on('selected', selectedObjectEvent);
            canvas.add(triangle);
        });

        $('#lineBtn').on('click', function(){
            var line = new fabric.Line([ 250, 125, 250, 175 ], {
                fill: 'red',
                stroke: 'red',
                strokeWidth: 5,
                selectable: true
            });

            line.on('selected', selectedObjectEvent);
            canvas.add(line);
        });

        $('#addTextBtn').on('click', function(){
            var text = new fabric.IText('Tap and Type', {
                fontFamily: 'arial black',
                left: 100,
                top: 100
            });

            text.on('selected', selectedTextEvent);
            canvas.add(text);
        });

        // Spectrum Color Picker

        $("#SpectrumPicker").spectrum({
            color: "#f00",
            move: function(color) {
                canvas.setBackgroundColor(color.toRgbString());
                console.log('move' + color.toRgbString());
                canvas.renderAll();

            },
            hide: function(color) {
                canvas.setBackgroundColor(color.toRgbString());
                console.log('hide' + color.toRgbString());
                canvas.renderAll();

            }
        });

        $("#SimpleShapesColor").spectrum({
            color: "#f00",
            move: function (color) {
                canvas.getActiveObject().setColor(color.toRgbString());
                canvas.renderAll();
            },
            hide: function (color) {
                canvas.getActiveObject().setColor(color.toRgbString());
                canvas.renderAll();
            }
        });

        $('#TextColor').spectrum({
            color: "#f00",
            move: function (color) {
                canvas.getActiveObject().setColor(color.toRgbString());
                canvas.renderAll();
            },
            hide: function (color) {
                canvas.getActiveObject().setColor(color.toRgbString());
                canvas.renderAll();
            }
        });

        $('#TextBackgroundTextColor').spectrum({
            color: "#f00",
            move: function (color) {
                canvas.getActiveObject().setTextBackgroundColor(color.toRgbString());
                canvas.renderAll();
            },
            hide: function (color) {
                canvas.getActiveObject().setTextBackgroundColor(color.toRgbString());
                canvas.renderAll();
            }
        });

        $('#TextStrokeColor').spectrum({
            color: "#f00",
            move: function (color) {
                canvas.getActiveObject().setStroke(color.toRgbString());
                canvas.renderAll();
            },
            hide: function (color) {
                canvas.getActiveObject().setStroke(color.toRgbString());
                canvas.renderAll();
            }
        });

        $('#LineColor').spectrum({
                    color: "#f00",
                    move: function (color) {
                        canvas.freeDrawingBrush.color = color.toRgbString();
                    },
                    hide: function (color) {
                        canvas.freeDrawingBrush.color = color.toRgbString();
                    }
        });
    });

      function resizeCanvas(){
          var canvasDiv = document.getElementById("canvas");
          canvasDiv.width = $("#parent").width();
      }

      function selectedDrawingObject() {
          $('#deleteSelectedBtn').removeClass('disabled');
      }

      function selectedTextEvent() {
          selectedClearedObjectEvent();
          $('#TextOptions').show();
          $('#deleteSelectedBtn').removeClass('disabled');
      }

      function selectedObjectEvent(){
          selectedClearedObjectEvent();
          $('#SimpleShapesOptions').show();
          $('#deleteSelectedBtn').removeClass('disabled');
      }

      function selectedClearedObjectEvent() {
          $('#SimpleShapesOptions').hide();
          $('#TextOptions').hide();
          $('#DrawingModeOptions').hide();
          $('#deleteSelectedBtn').addClass('disabled');
      }


    function formToJSON() {
        var data = JSON.stringify(canvas);
        console.log(data);

        return data;
    }
  </script>
  <style>
    .ui.grid {
      margin-top: 20px;
      margin-bottom: 20px;
    }
    .floated.button {
      margin-right: 20px;
    }

    .slider {
        height: 15px;
        width: 200px;
        padding-left: 5px;
    }
    #wrapper {
        min-height: 100%;
    }
  </style>
</head>

<body onresize="resizeCanvas()">

<div id="wrapper">
<!-- Import Header-->
<%@ include file="header.jspf" %>


<div class="ui grid">

  <div class="four wide column">
    <div class="ui vertical fluid tabular menu">
      <a href="<c:url value="/user/controlPanel/"/>" class="item">
        <spring:message code="UserControlPanelPage.TabGallery"/>
      </a>
      <a href="<c:url value="/user/controlPanel/PhotoAlbum/"/>" class="item active">
        <spring:message code="UserControlPanelPage.TabPhotoAlbum"/>
      </a>
    </div>
  </div>

  <div class="twelve wide stretched column">
    <div class="ui clearing segment">
      <spring:message code="UserControlPanelPage.PhotoAlbumMessage"/>
    </div>

      <!-- Next / previous photoalbum page -->
      <div class="ui clearing segment">
          <div id="previousBtn" class="ui left floated button"><spring:message code="Canvas.previousPage"/> </div>
          <div id="nextBtn" class="ui right floated button"><spring:message code="Canvas.nextPage"/> </div>
      </div>

      <!-- Common functions eg. background, clear-->
     <div class="ui clearing segment">
       <spring:message code="Canvas.BackgroundColor"/> &nbsp;&nbsp;&nbsp; <input type='text' id="SpectrumPicker" /> &nbsp;&nbsp;&nbsp;
       <div id="deleteSelectedBtn" class="ui disabled positive button"><spring:message code="Canvas.deleteSelected"/> </div>
       <div id="clearCanvasBtn" class="ui positive button"><spring:message code="Canvas.clearCanvas"/> </div>
       <div id="drawingModeBtn" class="ui positive button"><spring:message code="Canvas.drawingMode"/> </div>
     </div>

      <!-- Simple Shapes -->
      <div class="ui clearing segment">
          <div id="rectangleBtn" class="ui positive button"><spring:message code="Canvas.rectangle"/> </div>
          <div id="circleBtn" class="ui positive button"><spring:message code="Canvas.circle"/> </div>
          <div id="triangleBtn" class="ui positive button"><spring:message code="Canvas.triangle"/> </div>
          <div id="lineBtn" class="ui positive button"><spring:message code="Canvas.line"/> </div>
          <div id="addTextBtn" class="ui positive button"><spring:message code="Canvas.addText"/> </div>
      </div>

      <!-- SimpleShapesOptions Hide Menu-->
      <div id="SimpleShapesOptions" class="ui clearing segment">
          <spring:message code="Canvas.ColorPicker"/> &nbsp;&nbsp;&nbsp; <input type='text' id="SimpleShapesColor" /> <br/><br/>
          <spring:message code="Canvas.Opacity" /><div class="slider" id="slider"></div>
      </div>

      <!-- TextOptions Hide Menu-->
      <div id="TextOptions" class="ui clearing segment">
          <spring:message code="Canvas.FontFamily"/> &nbsp;&nbsp;&nbsp;
          <div class="ui compact menu">
              <div id="FontFamilyDropdown" class="ui simple dropdown item">
                  <spring:message code="Common.Chose"/>
                  <i class="dropdown icon"></i>
                  <div class="menu">
                      <div class="item" data-value="arial">Arial</div>
                      <div class="item" data-value="helvetica">Helvetica</div>
                      <div class="item" data-value="myriad pro">Myriad Pro</div>
                      <div class="item" data-value="delicious">Delicious</div>
                      <div class="item" data-value="verdana">Verdana</div>
                      <div class="item" data-value="georgia">Georgia</div>
                      <div class="item" data-value="courier">Courier</div>
                      <div class="item" data-value="comic sans ms">Comic Sans MS</div>
                      <div class="item" data-value="impact">Impact</div>
                      <div class="item" data-value="monaco">Monaco</div>
                      <div class="item" data-value="optima">Optima</div>
                      <div class="item" data-value="hoefler text">Hoefler Text</div>
                      <div class="item" data-value="plaster">Plaster</div>
                      <div class="item" data-value="engagement">Engagement</div>
                  </div>
              </div>
          </div>
          &nbsp;&nbsp;&nbsp; <spring:message code="Canvas.TextAlign"/> &nbsp;&nbsp;&nbsp;
          <div class="ui compact menu">
              <div id="TextAlignDropdown" class="ui simple dropdown item">
                  <spring:message code="Common.Chose"/>
                  <i class="dropdown icon"></i>
                  <div class="menu">
                      <div class="item" data-value="left"><spring:message code="Canvas.TextAlignLeft"/> </div>
                      <div class="item" data-value="center"><spring:message code="Canvas.TextAlignCenter"/> </div>
                      <div class="item" data-value="right"><spring:message code="Canvas.TextAlignRight"/> </div>
                      <div class="item" data-value="justify"><spring:message code="Canvas.TextAlignJustify"/> </div>
                  </div>
              </div>
          </div> <br/><br/>
          <spring:message code="Canvas.TextColor"/> &nbsp;&nbsp;&nbsp; <input type='text' id="TextColor" /> &nbsp;&nbsp;&nbsp;
          <spring:message code="Canvas.TextBackgroundTextColor"/> &nbsp;&nbsp;&nbsp; <input type='text' id="TextBackgroundTextColor" /> &nbsp;&nbsp;&nbsp;
          <spring:message code="Canvas.TextStrokeColor"/> &nbsp;&nbsp;&nbsp; <input type='text' id="TextStrokeColor" /> <br/><br/>

          <spring:message code="Canvas.StrokeWidth" /> <br/> <div class="slider" id="StrokeWidth"></div> <br/>
          <spring:message code="Canvas.FontSize" /> <br/> <div class="slider" id="FontSize"></div> <br/>
          <spring:message code="Canvas.LineHeight" /> <br/> <div class="slider" id="LineHeight"></div> <br/>
          <spring:message code="Canvas.Opacity" /> <br/> <div class="slider" id="TextOpacity"></div> <br/><br/>

          <div id="TextBold" class="ui checkbox">
              <input type="checkbox" name="TextBold">
              <label><spring:message code="Canvas.TextBold"/></label>
          </div> &nbsp;&nbsp;&nbsp;

          <div id="TextItalic" class="ui checkbox">
              <input type="checkbox" name="TextItalic">
              <label><spring:message code="Canvas.TextItalic"/></label>
          </div> &nbsp;&nbsp;&nbsp;

          <div id="TextUnderline" class="ui checkbox">
              <input type="checkbox" name="TextUnderline">
              <label><spring:message code="Canvas.TextUnderline"/></label>
          </div> &nbsp;&nbsp;&nbsp;

          <div id="TextLineThrough" class="ui checkbox">
              <input type="checkbox" name="TextLineThrough">
              <label><spring:message code="Canvas.TextLineThrough"/></label>
          </div> &nbsp;&nbsp;&nbsp;

          <div id="TextOverLine" class="ui checkbox">
              <input type="checkbox" name="TextOverLine">
              <label><spring:message code="Canvas.TextOverline"/></label>
          </div> &nbsp;&nbsp;&nbsp;
      </div>

      <!-- DrawingModeOptions, Hide Menu-->
      <div id="DrawingModeOptions" class="ui clearing segment">
          <spring:message code="Canvas.DrawingEq"/> &nbsp;&nbsp;&nbsp;
          <div class="ui compact menu">
              <div id="DrawingEqDropdown" class="ui simple dropdown item">
                  <spring:message code="Common.Chose"/>
                  <i class="dropdown icon"></i>
                  <div class="menu">
                      <div class="item" data-value="Pencil"><spring:message code="Canvas.Pencil"/></div>
                      <div class="item" data-value="Circle"><spring:message code="Canvas.Circle"/></div>
                      <div class="item" data-value="Spray"><spring:message code="Canvas.Spray"/></div>
                      <div class="item" data-value="Pattern"><spring:message code="Canvas.Pattern"/></div>
                      <div class="item" data-value="hline"><spring:message code="Canvas.Hline"/></div>
                      <div class="item" data-value="vline"><spring:message code="Canvas.Vline"/></div>
                      <div class="item" data-value="square"><spring:message code="Canvas.Square"/></div>
                      <div class="item" data-value="diamond"><spring:message code="Canvas.Diamond"/></div>
                      <div class="item" data-value="texture"><spring:message code="Canvas.Texture"/></div>
                  </div>
              </div>
          </div> <br/><br/>
          <spring:message code="Canvas.LineWidth" /> <br/> <div class="slider" id="LineWidth"></div> <br/>
          <spring:message code="Canvas.ShadowWidth" /> <br/> <div class="slider" id="ShadowWidth"></div> <br/>
          <spring:message code="Canvas.LineColor"/> &nbsp;&nbsp;&nbsp; <input type='text' id="LineColor" /> <br/>
      </div>

      <!-- Canvas -->
      <div id="parent" class="ui clearing segment">
          <canvas id="canvas" height="400px" style="border:1px solid #000000;">
          </canvas>
      </div>

    <!-- Tabular Menu-->
      <div class="ui top attached tabular menu">
        <jstl:forEach items="${userGalleries}" var="gallery">
            <a class="item" data-tab="${gallery.getGalleryName()}">${gallery.getGalleryName()}</a>
        </jstl:forEach>
      </div>

      <jstl:forEach items="${userGalleries}" var="gallery">
        <div class="ui bottom attached tab segment" data-tab="${gallery.getGalleryName()}">
          <jstl:forEach items="${gallery.getImages()}" var="photo">
            <div class="ui image segment">
                <div class="image">
                        <img class="ui small image draggable" src="<c:url value="/showImage?id=${photo.getId()}"/>" id="${photo.getFileName()}" />
                </div>
            </div>&nbsp;&nbsp;
          </jstl:forEach>
        </div>

      </jstl:forEach>

    </div>
</div>
</div>

<!-- Import Footer-->
<%@ include file="footer.jspf" %>

</body>
</html>
