<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="jstl" %>

<%@ include file="libraries.jspf" %>

<!-- Additional values -->
<spring:message code="UserControlPanelPage.SiteTitle" var="pageTitle"/>
<spring:url value="/resources/assets/noImage.png" var="noImage" />

<!DOCTYPE html>
<html>
<head>

  <link href="${SemanticCSS}" rel="stylesheet" />
  <link href="${themeCSS}" rel="stylesheet" />

  <script src="${jQuery}"></script>
  <script src="${jQueryUI}"></script>
  <script src="${jQuerySerialize}"></script>
  <script src="${i18props}"></script>

  <script src="${SemanticJS}"></script>
  <script src="${themeJS}"></script>

  <title>${pageTitle}</title>

  <script>
    $(document).ready(function() {

      $('#followMenu').hide();

      // fix menu when passed
      $('#MainHeader')
              .visibility({
                once: false,
                onBottomPassed: function() {
                  $('#followMenu').transition('fade in');
                },
                onBottomPassedReverse: function() {
                  $('#followMenu').transition('fade out');
                }
              })
      ;

      $('.ui.modal')
              .modal({
                blurring: true,
                closable  : false,
                onDeny    : function(){
                  $('#addPhotoAlbumModal').find('input').val(''); /*clear fields*/
                },
                onApprove : function() {
                  var name = $('#modalInput').find('input[name="photoAlbumName"]').val();
                  var PhotoAlbumId;

                  $.ajax({
                    url: '<c:url value="/user/controlPanel/addPhotoAlbum"/>',
                    type: "post",
                    data: formToJSON(),
                    dataType : "json",
                    contentType: 'application/json; charset=utf-8',
                    async: false,
                    success : function(data){
                      PhotoAlbumId = data.message;

                      console.log(data);
                    },
                    error : function(xhr, status){
                      console.log(status);
                    }
                  });

                  console.log(PhotoAlbumId);

                  var url = PhotoAlbumId +'/1/';
                  var deleteUrl = 'delete/' + PhotoAlbumId;
                  var previewUrl = 'preview/' + PhotoAlbumId;

                  var newGalleryHTML = '<div class="column">'
                          + '<div class="ui image segment">'
                          + '<div class="ui black ribbon label">'
                          + '<i class="Comment Outline icon"></i>' + name
                          + '</div>'
                          + '<a href="' + url + '">'
                          + '<img src="'+ '<c:url value="/assets/noImage.png"/>' +'" alt="' + name + '">'
                          + '</a>'
                          + '</div>'
                          + '<a href="' + deleteUrl + '">'
                          + '<button class="negative ui button">'
                          + '<spring:message code="Common.Delete"/>'
                          + '</button></a>'
                          + '<a href="' + previewUrl + '">'
                          + '<button class="positive ui button">'
                          + '<spring:message code="Common.Preview"/>'
                          + '</button></a>'
                          + '</div>;';

                  console.log(newGalleryHTML);

                  var $newGallery = $(newGalleryHTML);
                  $newGallery.hide();

                  $('#photoAlbumGrid').prepend($newGallery);

                  $newGallery.show('clip', 1000);

                  $('#addPhotoAlbumModal').find('input').val(''); /*clear fields*/
                }
              })
      ;

      $('.floated.button').on('click', function(){
        $('.ui.modal').modal('show');
      })
      ;

    });

    // pack ui.form data to JSON format
    function formToJSON() {
      var text = $('#modalInput').find('input[name="photoAlbumName"]').val();
      console.log(text);

      return JSON.stringify({
        "photoAlbumName": text
      })
    }
  </script>
  <style>
    .ui.grid {
      margin-top: 20px;
      margin-bottom: 20px;
    }
    .floated.button {
      margin-right: 20px;
    }
    #wrapper {
      min-height: 100%;
    }
  </style>
</head>

<body>

<div id="wrapper">
<!-- Import Header-->
<%@ include file="header.jspf" %>

<!-- Add gallery dialog box -->
<div class="ui modal" id="addPhotoAlbumModal">
  <i class="close icon"></i>
  <div class="header">
    <spring:message code="UserControlPanelPage.AddPhotoAlbumModalTitle"/>
  </div>

  <div class="content">
    <div class="ui fluid input" id="modalInput">
      <input name="photoAlbumName" type="text" placeholder="<spring:message code="UserControlPanelPage.AddPhotoAlbumModalInputPlaceHolder"/> ">
    </div>
  </div>

  <div class="actions">
    <div class="ui cancel button"><spring:message code="UserControlPanelPage.AddGalleryModalCancelBtn"/> </div>
    <div class="ui approve button"><spring:message code="UserControlPanelPage.AddGalleryModalOKBtn"/> </div>
  </div>

</div>

<div class="ui grid">
  <div class="four wide column">
    <div class="ui vertical fluid tabular menu">
      <a href="<c:url value="/user/controlPanel/"/>" class="item">
        <spring:message code="UserControlPanelPage.TabGallery"/>
      </a>
      <a href="<c:url value="/user/controlPanel/PhotoAlbum/"/>" class="item active">
        <spring:message code="UserControlPanelPage.TabPhotoAlbum"/>
      </a>
    </div>
  </div>
  <div class="twelve wide stretched column">
    <div class="ui clearing segment">
      <spring:message code="UserControlPanelPage.PhotoAlbumMessage"/>

      <div class="ui right floated button"><spring:message code="UserControlPanelPage.AddGalleryModalShowModal"/> </div>
    </div>
    <div class="ui three column grid" id="photoAlbumGrid">

      <jstl:forEach items="${photoAlbums}" var="photoAlbum">
        <div class="column">
          <div class="ui image segment">
            <div class="ui black ribbon label">
              <i class="Comment Outline icon"></i> <jstl:out value="${photoAlbum.getPhotoAlbumName()}"/>
            </div>
            <a href="<jstl:url value="/user/controlPanel/PhotoAlbum/${photoAlbum.getId()}/1/"/>">
              <jstl:if test="${empty photoAlbum.getPhotoAlbumPages()}">
                <img src="<c:url value="/assets/noImage.png"/>" alt=<jstl:out value="${photoAlbum.getPhotoAlbumName()}"/>>
              </jstl:if>

              <jstl:if test="${not empty photoAlbum.getPhotoAlbumPages()}">
                <img src="<c:url value="/showImage/photoalbumpage?id=${photoAlbum.getPhotoAlbumPages().stream().findFirst().get().getId()}"/>" alt="${photoAlbum.getPhotoAlbumName()}" />
              </jstl:if>
            </a>
          </div>
          <a href="<c:url value="delete/${photoAlbum.getId()}"/>">
            <button class="negative ui button">
              <spring:message code="Common.Delete"/>
            </button></a>
          <a href="<c:url value="preview/${photoAlbum.getId()}"/>">
            <button class="positive ui button">
              <spring:message code="Common.Preview"/>
            </button></a>
        </div>
      </jstl:forEach>

    </div>
  </div>
</div>
</div>

<!-- Import Footer-->
<%@ include file="footer.jspf" %>

</body>
</html>
