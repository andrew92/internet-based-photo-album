<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ include file="libraries.jspf" %>

<!-- Additional values -->
<spring:message code="Index.SiteTitle" var="pageTitle"/>

<!DOCTYPE html>
<html>
  <head>

      <link href="${SemanticCSS}" rel="stylesheet" />
      <link href="${themeCSS}" rel="stylesheet" />
      <link href="${OwlCarouselCSS}" rel="stylesheet" />
      <link href="${OwlCarouselThemeCss}" rel="stylesheet" />

      <script src="${jQuery}"></script>
      <script src="${jQuerySerialize}"></script>
      <script src="${i18props}"></script>
      <script src="${OwlCarousel}"></script>

      <script src="${SemanticJS}"></script>
      <script src="${themeJS}"></script>

      <title>${pageTitle}</title>

      <script>
          $(document).ready(function() {

              $("#carousel").owlCarousel({
                  // Most important owl features
                  items : 1,

                  itemsCustom : false,
                  itemsDesktop : [1199,4],
                  itemsDesktopSmall : [980,3],
                  itemsTablet: [768,2],
                  itemsTabletSmall: false,
                  itemsMobile : [479,1],
                  singleItem : false,
                  itemsScaleUp : false,

                  //Basic Speeds
                  slideSpeed : 200,
                  paginationSpeed : 800,
                  rewindSpeed : 1000,

                  //Autoplay
                  autoPlay : true,
                  stopOnHover : true,

                  // Navigation
                  navigation : false,
                  navigationText : ["prev","next"],
                  rewindNav : false,
                  scrollPerPage : false,

                  //Pagination
                  pagination : false,
                  paginationNumbers: false,

                  // Responsive
                  responsive: true,
                  responsiveRefreshRate : 200,
                  responsiveBaseWidth: window,

                  // CSS Styles
                  baseClass : "owl-carousel",
                  theme : "owl-theme",

                  //Lazy load
                  lazyLoad : false,
                  lazyFollow : true,
                  lazyEffect : "fade",

                  //Auto height
                  autoHeight : false,

                  //JSON
                  jsonPath : false,
                  jsonSuccess : false,

                  //Mouse Events
                  dragBeforeAnimFinish : true,
                  mouseDrag : true,
                  touchDrag : true,

                  //Transitions
                  transitionStyle : false,

                  // Other
                  addClassActive : false,

                  //Callbacks
                  beforeUpdate : false,
                  afterUpdate : false,
                  beforeInit: false,
                  afterInit: false,
                  beforeMove: false,
                  afterMove: false,
                  afterAction: false,
                  startDragging : false,
                  afterLazyLoad : false
              });

              $('#followMenu').hide();

              // fix menu when passed
              $('#MainHeader')
                      .visibility({
                          once: false,
                          onBottomPassed: function() {
                              $('#followMenu').transition('fade in');
                          },
                          onBottomPassedReverse: function() {
                              $('#followMenu').transition('fade out');
                          }
                      })
              ;

          })
      </script>

      <style>
          #carousel .item img{
              display: block;
              width: 100%;
              height: 600px;
          }

          .ui.vertical.stripe {
              padding: 8em 0em;
          }
          .ui.vertical.stripe h3 {
              font-size: 2em;
          }
          .ui.vertical.stripe .button + h3,
          .ui.vertical.stripe p + h3 {
              margin-top: 3em;
          }
          .ui.vertical.stripe .floated.image {
              clear: both;
          }
          .ui.vertical.stripe p {
              font-size: 1.33em;
          }
          .ui.vertical.stripe .horizontal.divider {
              margin: 3em 0em;
          }

          .quote.stripe.segment {
              padding: 0em;
          }
          .quote.stripe.segment .grid .column {
              padding-top: 5em;
              padding-bottom: 5em;
          }
      </style>

  </head>

  <body>

  <!-- Import Header-->
  <%@ include file="header.jspf" %>

  <!-- Owl Carousel -->
  <div id="carousel">
      <div class="item"><img src="assets/img1.jpg" alt="Image"></div>
      <div class="item"><img src="assets/img2.jpg" alt="Image"></div>
      <div class="item"><img src="assets/img3.jpg" alt="Image"></div>
      <div class="item"><img src="assets/img4.jpg" alt="Image"></div>
      <div class="item"><img src="assets/img5.jpg" alt="Image"></div>
      <div class="item"><img src="assets/img6.jpg" alt="Image"></div>
  </div>
  <!-- END Carousel -->

  <div class="ui vertical stripe segment" id="smth">
      <div class="ui middle aligned stackable grid container">
          <div class="row">
              <div class="eight wide column">
                  <h3 class="ui header"><spring:message code="Index.AdvertisementHeader1"/> </h3>
                  <p><spring:message code="Index.AdvertisementHeader1Text"/> </p>
                  <h3 class="ui header"><spring:message code="Index.AdvertisementHeader2"/> </h3>
                  <p><spring:message code="Index.AdvertisementHeader2Text"/> </p>
              </div>
              <div class="six wide right floated column">
                  <img src="assets/selfie.jpg" class="ui large bordered rounded image">
              </div>
          </div>
          <div class="row">
              <div class="center aligned column">
                  <a href="<c:url value="/user/register/"/>" class="ui huge button"><spring:message code="Index.AdvertisementButton"/> </a>
              </div>
          </div>
      </div>
  </div>

  <!--
    <br/>
  <security:authorize access="hasRole('ROLE_GUEST')">
      Not logged in
      <br/>
  </security:authorize>

  <security:authorize access="hasRole('ROLE_USER')">
      Logged in User, Has Role
      <br/>
  </security:authorize>

  <security:authorize access="hasAuthority('READ_PRIVILEGE')">
      Logged in User, Has Authority
      <br/>
  </security:authorize>

  <security:authorize access="hasRole('READ_PRIVILEGE')">
      User, Logged in
      <br/>
      <security:authentication property="principal.authorities"/>
  </security:authorize>
-->

  <!-- Import Footer-->
  <%@ include file="footer.jspf" %>

  </body>
</html>
