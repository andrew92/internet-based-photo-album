<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://www.springframework.org/tags" %>
<%@ page contentType="text/html;charset=ISO-8859-2" language="java" %>

<%@ include file="libraries.jspf" %>

<!-- Additional values -->
<spring:message code="InvalidToken.SiteTitle" var="pageTitle"/>

<!DOCTYPE html>
<html>
<head>

  <link href="${SemanticCSS}" rel="stylesheet" />
  <link href="${themeCSS}" rel="stylesheet" />

  <script src="${jQuery}"></script>
  <script src="${jQuerySerialize}"></script>
  <script src="${i18props}"></script>

  <script src="${SemanticJS}"></script>
  <script src="${themeJS}"></script>

  <title>${pageTitle}</title>

  <style>
    /* Form css */
    body {
      background-color: #DADADA;
    }
    body > .grid {
      height: 100%;
    }
    .ui.info.message {
      margin-top: auto;
      margin-bottom: auto;
    }
  </style>
</head>

<body>
<div class="ui middle aligned center aligned grid">
<div class="ui info message">
  ${message}
</div>
</div>
</body>

</html>
