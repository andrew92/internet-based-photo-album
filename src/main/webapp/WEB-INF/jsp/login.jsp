<%@ page import="org.springframework.web.servlet.support.RequestContextUtils" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://www.springframework.org/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ include file="libraries.jspf" %>

<!-- Additional values -->
<spring:message code="LoginPage.SiteTitle" var="pageTitle"/>


<!DOCTYPE html>
<html>
<head>

  <link href="${SemanticCSS}" rel="stylesheet" />


  <script src="${jQuery}"></script>
  <script src="${jQuerySerialize}"></script>
  <script src="${i18props}"></script>

  <script src="${SemanticJS}"></script>
  <script src="${themeJS}"></script>

  <title>${pageTitle}</title>

  <style>
    /* Form css */
    body {
      background-color: #DADADA;
    }
    body > .grid {
      height: 100%;
    }
    .image {
      margin-top: -100px;
    }
    .column {
      max-width: 450px;
    }
  </style>

  <script>
    // get system locale for i18n
    function getSystemLocale() {
      var systemLocale ='<%=RequestContextUtils.getLocale(request)%>';
      return systemLocale;
    }

    $(document).ready(function() {

      // init i18n.properties plugin
      $.i18n.properties({
        name:'JS_Messages',
        path: '${JSMessages}/',
        mode:'both',
        language:getSystemLocale(),
        encoding: 'Windows-1250',
        callback: function() {
          console.log("Configured")
        }
      });

      // init .ui.form validation
      $('.ui.form')
              .form({
                on: 'blur',
                fields: {
                  j_username: {
                    identifier: 'j_username',
                    rules: [
                      {
                        type: 'email',
                        prompt: jQuery.i18n.prop('LoginForm.InvalidEmail')
                      }
                    ]
                  },
                  j_password: {
                    identifier: 'j_password',
                    rules: [
                      {
                        type: 'regExp[/((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%]).{6,20})/]',
                        prompt: jQuery.i18n.prop('LoginForm.InvalidPassword')
                      }
                    ]
                  }
                }
              })
      ;

    })
    ;
  </script>
</head>

<body>

<div class="ui middle aligned center aligned grid">

  <div class="column">

    <h2 class="ui teal image header">

      <div class="content">
        <spring:message code="LoginPage.HeaderText"/>
      </div>
    </h2>

    <form class="ui large form" action="j_spring_security_check" method="post">
      <div class="ui stacked segment">

        <div class="field">
          <div class="ui left icon input">
            <i class="mail outline icon"></i>
            <input type="text" name="j_username" placeholder='<spring:message code="LoginPage.Email"/>' >
          </div>
        </div>

        <div class="field">
          <div class="ui left icon input">
            <i class="lock icon"></i>
            <input type="password" name="j_password" placeholder='<spring:message code="LoginPage.Password"/>' >
          </div>
        </div>

        <div class="ui fluid large teal submit button"><spring:message code="LoginPage.LoginButton" /> </div>

      </div>

      <div class="ui error message"></div>

    </form>

      <div class="ui info message">
          <ul class="list">
              <li><spring:message code="LoginPage.RegisterPageRedirect"/> <a href="<c:url value="/user/register/"/>"> <spring:message code="RegisterPage.RegisterButton"/> </a></li>
              <li><spring:message code="LoginPage.ResendPassword"/> <a href="<c:url value="/user/resetPassword/"/>"> <spring:message code="LoginPage.ChangePasswordButton"/> </a></li>
              <li><spring:message code="LoginPage.ResendActivationCode"/> <a href="<c:url value="/user/resendActivationToken/"/>"> <spring:message code="LoginPage.ResendActivationToken"/> </a></li>
          </ul>
      </div>
</div>

</div>
</body>
</html>