<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="jstl" %>

<%@ include file="libraries.jspf" %>

<!-- Additional values -->
<spring:message code="UserControlPanelPage.SiteTitle" var="pageTitle"/>
<spring:url value="/resources/assets/noImage.png" var="noImage" />

<!DOCTYPE html>
<html>
<head>

  <link href="${SemanticCSS}" rel="stylesheet" />
  <link href="${themeCSS}" rel="stylesheet" />

  <script src="${jQuery}"></script>
    <script src="${jQueryUI}"></script>
  <script src="${jQuerySerialize}"></script>
  <script src="${i18props}"></script>

  <script src="${SemanticJS}"></script>
  <script src="${themeJS}"></script>

  <title>${pageTitle}</title>

  <script>
    $(document).ready(function() {

      $('#followMenu').hide();

      // fix menu when passed
      $('#MainHeader')
              .visibility({
                once: false,
                onBottomPassed: function() {
                  $('#followMenu').transition('fade in');
                },
                onBottomPassedReverse: function() {
                  $('#followMenu').transition('fade out');
                }
              })
      ;

        $('.ui.modal')
                .modal({
                    blurring: true,
                    closable  : false,
                    onDeny    : function(){
                        $('#addGalleryModal').find('input').val(''); /*clear fields*/
                        // window.alert('Wait not yet!');
                    },
                    onApprove : function() {
                        var name = $('#modalInput').find('input[name="galleryName"]').val();
                        var galleryId;

                            $.ajax({
                                url: '<c:url value="/user/controlPanel/addGallery"/>',
                                type: "post",
                                data: formToJSON(),
                                dataType : "json",
                                contentType: 'application/json; charset=utf-8',
                                async: false,
                                success : function(data){
                                    galleryId = data.message;

                                    console.log(data);
                                },
                                error : function(xhr, status){
                                    console.log(status);
                                }
                            });

                        console.log(galleryId);

                        var url = 'Gallery/' + galleryId +'/';
                        var deleteUrl = 'delete/' + galleryId;

                        /*var newGalleryHTML = '<div class="column">'
                                + '<div class="ui segment">'
                                + '<a href="' + url + '">'
                                + '<img class="ui centered small image" src="'+ '<c:url value="/assets/noImage.png"/>' +'" alt="' + name + '">'
                                + '</a>'
                                + '</div>'
                                + '</div>'; */

                        var newGalleryHTML = '<div class="column">'
                                + '<div class="ui image segment">'
                                + '<div class="ui black ribbon label">'
                                + '<i class="Comment Outline icon"></i>' + name
                                + '</div>'
                                + '<a href="' + url + '">'
                                + '<img src="'+ '<c:url value="/assets/noImage.png"/>' +'" alt="' + name + '">'
                                + '</a>'
                                + '</div>'
                                + '<a href="' + deleteUrl + '">'
                                + '<button class="negative ui button">'
                                + '<spring:message code="Common.Delete"/>'
                                + '</button></a>'
                                + '</div>;';

                        console.log(newGalleryHTML);

                        var $newGallery = $(newGalleryHTML);
                        $newGallery.hide();

                        $('#galleryGrid').prepend($newGallery);

                        $newGallery.show('clip', 1000);

                        $('#addGalleryModal').find('input').val(''); /*clear fields*/
                    }
                })
        ;

        $('.floated.button').on('click', function(){
            $('.ui.modal').modal('show');
        })
        ;

    })

    // pack ui.form data to JSON format
    function formToJSON() {
        var text = $('#modalInput').find('input[name="galleryName"]').val();
        console.log(text);

        return JSON.stringify({
            "galleryName": text
        })
    }
  </script>
  <style>
      .ui.grid {
          margin-top: 20px;
          margin-bottom: 20px;
      }
      .floated.button {
          margin-right: 20px;
      }

      #wrapper {
          min-height: 100%;
      }
  </style>
</head>

<body>

<!-- Add gallery dialog box -->
<div class="ui modal" id="addGalleryModal">
    <i class="close icon"></i>
    <div class="header">
        <spring:message code="UserControlPanelPage.AddGalleryModalTitle"/>
    </div>

    <div class="content">
    <div class="ui fluid input" id="modalInput">
        <input name="galleryName" type="text" placeholder="<spring:message code="UserControlPanelPage.AddGalleryModalInputPlaceHolder"/> ">
    </div>
    </div>

    <div class="actions">
        <div class="ui cancel button"><spring:message code="UserControlPanelPage.AddGalleryModalCancelBtn"/> </div>
        <div class="ui approve button"><spring:message code="UserControlPanelPage.AddGalleryModalOKBtn"/> </div>
    </div>

</div>

<div id="wrapper">
<!-- Import Header-->
<%@ include file="header.jspf" %>


<div class="ui grid">
    <div class="four wide column">
        <div class="ui vertical fluid tabular menu">
            <a href="<c:url value="/user/controlPanel/"/>" class="item active">
                <spring:message code="UserControlPanelPage.TabGallery"/>
            </a>
            <a href="<c:url value="/user/controlPanel/PhotoAlbum/"/>" class="item">
                <spring:message code="UserControlPanelPage.TabPhotoAlbum"/>
            </a>
        </div>
    </div>
    <div class="twelve wide stretched column">
        <div class="ui clearing segment">
            <spring:message code="UserControlPanelPage.GalleryMessage"/>

            <div class="ui right floated button"><spring:message code="UserControlPanelPage.AddGalleryModalShowModal"/> </div>
        </div>
        <div class="ui three column grid" id="galleryGrid">

            <jstl:forEach items="${userGalleries}" var="gallery">
                <div class="column">
                    <div class="ui image segment">
                        <div class="ui black ribbon label">
                            <i class="Comment Outline icon"></i> <jstl:out value="${gallery.getGalleryName()}"/>
                        </div>
                        <a href="<jstl:url value="Gallery/${gallery.getId()}/"/>">
                            <jstl:if test="${empty gallery.getImages()}">
                              <img src="<c:url value="/assets/noImage.png"/>" alt=<jstl:out value="${gallery.getGalleryName()}"/>>
                            </jstl:if>

                            <jstl:if test="${not empty gallery.getImages()}">
                                <img src="<c:url value="/showImage?id=${gallery.getImages().stream().findFirst().get().getId()}"/>" alt=<jstl:out value="${gallery.getGalleryName()}"/>>
                            </jstl:if>
                        </a>
                    </div>
                    <a href="<c:url value="delete/${gallery.getId()}"/>">
                    <button class="negative ui button">
                       <spring:message code="Common.Delete"/>
                    </button></a>
                </div>
            </jstl:forEach>

        </div>
    </div>
</div>
</div>
<!-- Import Footer-->
<%@ include file="footer.jspf" %>

</body>
</html>
