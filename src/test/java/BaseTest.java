import com.photoalbum.dto.User;
import com.photoalbum.dto.form.UserDTO;
import com.photoalbum.service.UserServiceImpl;
import org.junit.Before;

import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

/**
 * Created by Andrzej Olkiewicz on 2016-03-20.
 */
public class BaseTest {

    @Mock
    UserServiceImpl userService;

    @Before
    public void setUp(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void test(){
        assertTrue(true);
    }

    @Test
    public void test2(){
        assertThat(new Object(), is(instanceOf(Object.class)));
    }

    @Test
    public void test3(){
        when(userService.createUser(new UserDTO())).thenReturn(new User());

        assertThat(userService.createUser(new UserDTO()), is(instanceOf(User.class)));
    }
}
